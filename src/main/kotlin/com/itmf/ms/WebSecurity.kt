package com.itmf.ms

import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter

@Configuration
@EnableWebSecurity
class WebSecurity : WebSecurityConfigurerAdapter() {

	override
	fun configure(http: HttpSecurity) {
		http.csrf()
			.disable()
			.authorizeRequests()
			.anyRequest().authenticated()
			.and()
			.httpBasic()
	}

}